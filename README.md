# BoaDiversão - Teste

## Instruções para instalação do sistema
O projeto foi desenvolvido utilizando o framework PHP **Laravel**. O Laravel utiliza do gerenciador de pacotes/depedências chamado **composer**.

- Para instalar o **composer** clique em https://getcomposer.org/download/ e faça o download para a versão do seu sistema operacional.

- Agora clone o código do projeto executando o comando no terminal/cmd/bash:
    `` git clone https://gitlab.com/lucaspanik/teste-boadiversao.git ``

- Vá até a pasta do projeto, com o **composer** instalado abra o terminal/cmd/bash e digite o seguinte comando:
 `` composer install ``
Este comando irá instalar todas as bibliotecas que o projeto depende para funcionar.

- Após a instalação das bibliotecas via **composer**, renomeie o arquivo **.env.example** (que se econtra na raiz da pasta do projeto) para **.env**, abra o arquivo e informe as configurações de conexão com banco de dados:
    - DB_CONNECTION= tipo de banco de dados (padrão: **mysql**)
    - DB_HOST= servidor do banco de dados(padrão: **localhost**)
    - DB_PORT= porta do servidor de banco de dados (padrão: **3306**)
    - DB_DATABASE= nome do banco de dados a ser utilizado
    - DB_USERNAME= nome de usuário de conexão com o banco de dados
    - DB_PASSWORD= senha do usuário de conexão com o banco de dados

- Após a configuração do banco de dados ser realizada no arquivo **.env** execute o seguinte comando no terminal/cmd/bash:
 ``php artisan migrate``
 Este comando irá realizar as migrações do schema do banco de dados criando todas as tabelas necessárias para seu correto funcionamento.

 > **Em caso de erro com o comando "php não é reconhecido como um comando interno" no windows, basta adicionar o caminho do seu interpretador PHP na variável de ambiente PATH** (botão direito em Meu Computador => Propriedades => Alterar Configurações => Aba "Avançado" => Variáveis de Ambiente => Path/PATH)

- Após realizar as migrações necessárias para montar a estrutura do banco de dados, execute o seguinte comando no terminal/cmd/bash:
  ``php artisan db:seed ``
  Este comando irá popular o banco de dados com informações previamente devinidas no briefing do teste.

- Agora execute o comando abaixo no terminal/cmd/bash para iniciar o servidor:
 ``php artisan serve``
O sistema deverá ficar disponível no endereço: http://localhost:8000

- Agora clique sobre o menu superior "Registrar" e efetue o cadastro de um novo usuário. (função habilitada para facilitar a avaliação do projeto)


# Funcionalidades
  - Registro de Usuários
  - Login de Usuários
  - Gerenciamento de Sexos
  - Gerenciamento de Setores
  - Gerenciamento de Tipos de Entrada
  - Gerenciamento de Lotes
  - Gerenciamento de Ingressos
  - Registro de Entradas (com ingressos)

# Sobre o Sistema
- Após o cadastro no sistema, você será redirecionado para a tela de "Gerenciamento de Ingressos", nela poderá ser cadastrados novos ingressos para servir de base no momento de "Registrar uma entrada".
- No menu Configurações (topo do sistema) se encontra os gerenciamentos das tabelas auxiliares para criação de um ingresso.
- Na tela de "Registrar Entrada" se encontra todos os ingressos previamente cadastrados, onde há a possibilidade de registrar entradas para ingressos distintos.

# Frameworks
- Bootstrap 3 (Front-end)
- Laravel 5.4 (Back-end)