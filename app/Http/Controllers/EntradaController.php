<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Entrada;

class EntradaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // Obtendo todos os elementos do banco de dados
        $entradas = Entrada::all();

        // redirecionando para a view após a inserção do registro
        return view("entrada.index", ["entradas" => $entradas]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // redirecionando para a view para inserção das informações
        return view("entrada.element");
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // Obtendo informação do formulário
        $entradaInput = $request->input("entrada");

        // Inserindo no bando de dados
        $insert = Entrada::create(['entrada' => $entradaInput]);

        // redirecionando para a view após a inserção do registro
        if ($insert)
            return redirect()->route("entrada.index", ["success" => "Entrada cadastrada com sucesso"]);
        else
            return redirect()->route("entrada.index", ["error" => "Oopz: Erro ao cadastrar entrada."]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        // Obtendo o registro pelo ID
        $entrada = Entrada::find($id);

        // redirecionando para a view após a recuperação do registro
        return view("entrada.element", ['entrada' => $entrada]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // Obtendo o registro pelo ID
        $entrada = Entrada::find($id);

        // Obtendo informação do formulário
        $entradaInput = $request->input("entrada");

        // Inserindo no bando de dados
        $update = $entrada->update(['entrada' => $entradaInput]);

        // redirecionando para a view após a edição do registro
        if ($update)
            return redirect()->route("entrada.index", ["success" => "Entrada alterada com sucesso"]);
        else
            return redirect()->route("entrada.index", ["error" => "Oopz: Erro ao alterar entrada."]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
