<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Ingresso;
use App\Models\Setor;
use App\Models\Sexo;
use App\Models\Lote;
use App\Models\Entrada;


class IngressoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // Obtendo todos os elementos do banco de dados
        $ingressos = Ingresso::all();

        // redirecionando para a view após a inserção do registro
        return view("ingresso.index", compact("ingressos"));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // Obtendo todos os elementos do banco de dados
        $ingressos = Ingresso::all();
        $setores   = Setor::all();
        $sexos     = Sexo::all();
        $lotes     = Lote::all();
        $entradas  = Entrada::all();

        // redirecionando para a view para inserção das informações
        return view("ingresso.element", compact("setores", "sexos", "lotes", "entradas"));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // Obtendo informação do formulário
        $inputSetor   = $request->input("setor");
        $inputSexo    = $request->input("sexo");
        $inputLote    = $request->input("lote");
        $inputEntrada = $request->input("entrada");
        $inputValor   = $request->input("valor");

        // Inserindo no bando de dados
        $insert = Ingresso::create([
            "setor_id"   => $inputSetor,
            "sexo_id"    => $inputSexo,
            "lote_id"    => $inputLote,
            "entrada_id" => $inputEntrada,
            "valor"      => $inputValor
        ]);

        // redirecionando para a view após a inserção do registro
        if ($insert)
            return redirect()->route("ingresso.index", ["success" => "Ingresso cadastrado com sucesso"]);
        else
            return redirect()->route("ingresso.index", ["error" => "Oopz: Erro ao cadastrar ingresso."]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        // Obtendo o registro pelo ID
        $ingresso = Ingresso::find($id);

        // Obtendo todos os elementos do banco de dados
        $ingressos = Ingresso::all();
        $setores   = Setor::all();
        $sexos     = Sexo::all();
        $lotes     = Lote::all();
        $entradas  = Entrada::all();

        // redirecionando para a view após a recuperação do registro
        return view("ingresso.element", compact("ingresso", "setores", "sexos", "lotes", "entradas"));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // Obtendo o registro pelo ID
        $ingresso = Ingresso::find($id);

        // Obtendo informação do formulário
        $inputSetor   = $request->input("setor");
        $inputSexo    = $request->input("sexo");
        $inputLote    = $request->input("lote");
        $inputEntrada = $request->input("entrada");
        $inputValor   = $request->input("valor");

        // Inserindo no bando de dados
        $update = $ingresso->update([
            "setor_id"   => $inputSetor,
            "sexo_id"    => $inputSexo,
            "lote_id"    => $inputLote,
            "entrada_id" => $inputEntrada,
            "valor"      => $inputValor
        ]);

        // redirecionando para a view após a edição do registro
        if ($update)
            return redirect()->route("ingresso.index", ["success" => "Lote alterado com sucesso"]);
        else
            return redirect()->route("ingresso.index", ["error" => "Oopz: Erro ao alterar lote."]);

    }
}
