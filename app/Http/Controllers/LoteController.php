<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Lote;

class LoteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // Obtendo todos os elementos do banco de dados
        $lotes = Lote::all();

        // redirecionando para a view após a inserção do registro
        return view("lote.index", ["lotes" => $lotes]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // redirecionando para a view para inserção das informações
        return view("lote.element");
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // Obtendo informação do formulário
        $loteInput = $request->input("lote");

        // Inserindo no bando de dados
        $insert = Lote::create(['lote' => $loteInput]);

        // redirecionando para a view após a inserção do registro
        if ($insert)
            return redirect()->route("lote.index", ["success" => "Lote cadastrado com sucesso"]);
        else
            return redirect()->route("lote.index", ["error" => "Oopz: Erro ao cadastrar lote."]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        // Obtendo o registro pelo ID
        $lote = Lote::find($id);

        // redirecionando para a view após a recuperação do registro
        return view("lote.element", ['lote' => $lote]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // Obtendo o registro pelo ID
        $lote = Lote::find($id);

        // Obtendo informação do formulário
        $loteInput = $request->input("lote");

        // Inserindo no bando de dados
        $update = $lote->update(['lote' => $loteInput]);

        // redirecionando para a view após a edição do registro
        if ($update)
            return redirect()->route("lote.index", ["success" => "Lote alterado com sucesso"]);
        else
            return redirect()->route("lote.index", ["error" => "Oopz: Erro ao alterar lote."]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
