<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Registro;
use App\Models\Ingresso;

class RegistroController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // Obtendo todos os elementos do banco de dados
        $registros = Registro::orderBy('created_at', 'desc')->get();
        $ingressos = Ingresso::all();

        // redirecionando para a view após a inserção do registro
        return view("registro.index", compact("registros", "ingressos"));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        // Obtendo informação do formulário
        $ingressoInput = $request->input("ingresso");

        // Inserindo no bando de dados
        $insert = Registro::create(['ingresso_id' => $ingressoInput]);

        // redirecionando para a view após a inserção do registro
        if ($insert)
            return redirect()->route("registro.index", ["success" => "Registro cadastrado com sucesso"]);
        else
            return redirect()->route("registro.index", ["error" => "Oopz: Erro ao cadastrar registro."]);
    }
}
