<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Setor;

class SetorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // Obtendo todos os elementos do banco de dados
        $setores = Setor::all();

        // redirecionando para a view após a inserção do registro
        return view("setor.index", ["setores" => $setores]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // redirecionando para a view para inserção das informações
        return view("setor.element");
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // Obtendo informação do formulário
        $setorInput = $request->input("setor");

        // Inserindo no bando de dados
        $insert = Setor::create(['setor' => $setorInput]);

        // redirecionando para a view após a inserção do registro
        if ($insert)
            return redirect()->route("setor.index", ["success" => "Setor cadastrado com sucesso"]);
        else
            return redirect()->route("setor.index", ["error" => "Oopz: Erro ao cadastrar setor."]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     * /
    public function show($id)
    {
        //
    }
    */

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        // Obtendo o registro pelo ID
        $setor = Setor::find($id);

        // redirecionando para a view após a recuperação do registro
        return view("setor.element", ['setor' => $setor]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // Obtendo o registro pelo ID
        $setor = Setor::find($id);

        // Obtendo informação do formulário
        $setorInput = $request->input("setor");

        // Inserindo no bando de dados
        $update = $setor->update(['setor' => $setorInput]);

        // redirecionando para a view após a edição do registro
        if ($update)
            return redirect()->route("setor.index", ["success" => "Setor alterado com sucesso"]);
        else
            return redirect()->route("setor.index", ["error" => "Oopz: Erro ao alterar setor."]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     * /
    public function destroy($id)
    {
        //
    }
    */
}
