<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Sexo;

class SexoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // Obtendo todos os elementos do banco de dados
        $sexos = Sexo::all();

        // redirecionando para a view após a inserção do registro
        return view("sexo.index", ["sexos" => $sexos]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // redirecionando para a view para inserção das informações
        return view("sexo.element");
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // Obtendo informação do formulário
        $sexoInput = $request->input("sexo");

        // Inserindo no bando de dados
        $insert = Sexo::create(['sexo' => $sexoInput]);

        // redirecionando para a view após a inserção do registro
        if ($insert)
            return redirect()->route("sexo.index", ["success" => "Sexo cadastrado com sucesso"]);
        else
            return redirect()->route("sexo.index", ["error" => "Oopz: Erro ao cadastrar sexo."]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        // Obtendo o registro pelo ID
        $sexo = Sexo::find($id);

        // redirecionando para a view após a recuperação do registro
        return view("sexo.element", ['sexo' => $sexo]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // Obtendo o registro pelo ID
        $sexo = Sexo::find($id);

        // Obtendo informação do formulário
        $sexoInput = $request->input("sexo");

        // Inserindo no bando de dados
        $update = $sexo->update(['sexo' => $sexoInput]);

        // redirecionando para a view após a edição do registro
        if ($update)
            return redirect()->route("sexo.index", ["success" => "Sexo alterado com sucesso"]);
        else
            return redirect()->route("sexo.index", ["error" => "Oopz: Erro ao alterar sexo."]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
