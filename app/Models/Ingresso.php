<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Ingresso extends Model
{
    protected $fillable = [
        'setor_id', 'sexo_id', 'lote_id', 'entrada_id', 'valor'
    ];

    /**
     * Obtendo o registro do setor relacionado a este ingresso
     */
    public function setor()
    {
        return $this->belongsTo('App\Models\Setor');
    }

    /**
     * Obtendo o registro do sexo relacionado a este ingresso
     */
    public function sexo()
    {
        return $this->belongsTo('App\Models\Sexo');
    }

    /**
     * Obtendo o registro do lote relacionado a este ingresso
     */
    public function lote()
    {
        return $this->belongsTo('App\Models\Lote');
    }

    /**
     * Obtendo o registro do tipo de entrada relacionado a este ingresso
     */
    public function entrada()
    {
        return $this->belongsTo('App\Models\Entrada');
    }
}
