<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateIngressosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ingressos', function (Blueprint $table) {
            $table->increments('id');

            // Coluns
            $table->unsignedInteger('setor_id');
            $table->unsignedInteger('sexo_id');
            $table->unsignedInteger('lote_id');
            $table->unsignedInteger('entrada_id');

            // FKs
            $table->foreign('setor_id')->references('id')->on('setores')->onDelete('no action');
            $table->foreign('sexo_id')->references('id')->on('sexos')->onDelete('no action');
            $table->foreign('lote_id')->references('id')->on('lotes')->onDelete('no action');
            $table->foreign('entrada_id')->references('id')->on('entradas')->onDelete('no action');
            $table->decimal('valor', 8, 2);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ingressos');
    }
}
