<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UsersTableSeeder::class);
        $this->call(SetoresTableSeeder::class);
        $this->call(SexosTableSeeder::class);
        $this->call(LotesTableSeeder::class);
        $this->call(EntradasTableSeeder::class);
        $this->call(IngressosTableSeeder::class);
    }
}
