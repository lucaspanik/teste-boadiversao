<?php

use Illuminate\Database\Seeder;

class EntradasTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $arrayEntradas = array("Inteira", "Meia");
        foreach ($arrayEntradas as $entrada) {
            DB::table('entradas')->insert([
                'entrada' => $entrada,
                'created_at' => date("Y-m-d H:i:s"),
            ]);
        }
    }
}
