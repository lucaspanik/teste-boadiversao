<?php

use Illuminate\Database\Seeder;

class IngressosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('ingressos')->insert([
            'setor_id' => DB::table('setores')->inRandomOrder()->first()->id,
            'sexo_id' => DB::table('sexos')->inRandomOrder()->first()->id,
            'lote_id' => DB::table('lotes')->inRandomOrder()->first()->id,
            'entrada_id' => DB::table('entradas')->inRandomOrder()->first()->id,
            'valor' => number_format(rand(40, 120), 2),
            'created_at' => date("Y-m-d H:i:s"),
        ]);
    }
}
