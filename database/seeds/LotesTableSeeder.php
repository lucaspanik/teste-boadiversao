<?php

use Illuminate\Database\Seeder;

class LotesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $arrayLotes = array("1º Lote", "2º Lote", "3º Lote", "4º Lote", "5º Lote", "Promocional");
        foreach ($arrayLotes as $lote) {
            DB::table('lotes')->insert([
                'lote' => $lote,
                'created_at' => date("Y-m-d H:i:s"),
            ]);
        }
    }
}
