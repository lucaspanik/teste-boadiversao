<?php

use Illuminate\Database\Seeder;

class SetoresTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $arraySetores = array("Pista", "Área Vip", "Camarote");
        foreach ($arraySetores as $setor) {
            DB::table('setores')->insert([
                'setor' => $setor,
                'created_at' => date("Y-m-d H:i:s"),
            ]);
        }
    }
}
