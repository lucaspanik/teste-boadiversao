<?php

use Illuminate\Database\Seeder;

class SexosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $arraySexos = array("Masculino", "Feminino", "Unissex");
        foreach ($arraySexos as $sexo) {
            DB::table('sexos')->insert([
                'sexo' => $sexo,
                'created_at' => date("Y-m-d H:i:s"),
            ]);
        }
    }
}
