<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => "Lucas Moraes",
            'email' => 'lucas.panik@gmail.com',
            'password' => bcrypt('diversao@2017'),
            'created_at' => date("Y-m-d H:i:s"),
        ]);
    }
}
