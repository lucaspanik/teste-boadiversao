@if ( app('request')->input('success') != '' )
    <div class="alert alert-success" role="alert">{{ app('request')->input('success') }}</div>
@endif

@if ( app('request')->input('error') != '' )
    <div class="alert alert-danger" role="alert">{{ app('request')->input('error') }}</div>
@endif