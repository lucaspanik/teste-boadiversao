@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Gerenciamento de Ingressos</div>

                <div class="panel-body">

                    @if (isset($ingresso) )
                        <form action="{{ route('ingresso.update', $ingresso->id) }}" method="POST" role="form">
                        {!! method_field('PUT') !!}
                    @else
                        <form action="{{ route('ingresso.store') }}" method="POST" role="form">
                    @endif

                        {!! csrf_field() !!}

                        <div class="row">
                            <div class="col-xs-12 col-sm-6">
                                <div class="form-group">
                                    <label for="">Setor</label>
                                    <!-- <input type="text" class="form-control" name="ingresso" value="{{ $ingresso->ingresso or '' }}" required> -->
                                    <select class="form-control" name="setor" required>
                                        <option value=""></option>
                                        @foreach ($setores as $setor)
                                            <option value="{{ $setor->id }}" {{ (isset($ingresso) && $ingresso->setor_id == $setor->id ? "selected":"") }}>{{ $setor->setor }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-6">
                                <div class="form-group">
                                    <label for="">Sexo</label>
                                    <!-- <input type="text" class="form-control" name="ingresso" value="{{ $ingresso->ingresso or '' }}" required> -->
                                    <select class="form-control" name="sexo" required>
                                        <option value=""></option>
                                        @foreach ($sexos as $sexo)
                                            <option value="{{ $sexo->id }}" {{ (isset($ingresso) && $ingresso->sexo_id == $sexo->id ? "selected":"") }}>{{ $sexo->sexo }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-xs-12 col-sm-6">
                                <div class="form-group">
                                    <label for="">Lote</label>
                                    <!-- <input type="text" class="form-control" name="ingresso" value="{{ $ingresso->ingresso or '' }}" required> -->
                                    <select class="form-control" name="lote" required>
                                        <option value=""></option>
                                        @foreach ($lotes as $lote)
                                            <option value="{{ $lote->id }}" {{ (isset($ingresso) && $ingresso->lote_id == $lote->id ? "selected":"") }}>{{ $lote->lote }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-6">
                                <div class="form-group">
                                    <label for="">Entrada</label>
                                    <!-- <input type="text" class="form-control" name="ingresso" value="{{ $ingresso->ingresso or '' }}" required> -->
                                    <select class="form-control" name="entrada" required>
                                        <option value=""></option>
                                        @foreach ($entradas as $entrada)
                                            <option value="{{ $entrada->id }}" {{ (isset($ingresso) && $ingresso->entrada_id == $entrada->id ? "selected":"") }}>{{ $entrada->entrada }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-xs-12 col-sm-6">
                                <div class="form-group">
                                    <label for="">Valor</label>
                                    <input type="number" min="0.00" max="10000.00" step="0.50" class="form-control" name="valor" value="{{ $ingresso->valor or '0.00' }}" required>
                                </div>
                            </div>
                        </div>

                        <button type="submit" class="btn btn-success">Enviar</button>
                    </form>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
