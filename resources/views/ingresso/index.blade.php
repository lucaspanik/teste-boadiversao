@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Gerenciamento de Ingressos <a href="{{ route('ingresso.create') }}" class="btn btn-xs btn-success pull-right" alt="Adicionar" title="Adicionar"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span> Adicionar</a></div>

                <div class="panel-body">

                    @include('alertas')

                    <div class="table-responsive">
                        <table class="table table-hover">
                            <thead>
                                <tr>
                                    <th width="60">ID</th>
                                    <!-- <th>Ingresso</th> -->
                                    <th>Setor</th>
                                    <th>Sexo</th>
                                    <th>Lote</th>
                                    <th>Tipo de Entrada</th>
                                    <th>Valor</th>
                                    <th width="60">Ações</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($ingressos as $ingresso)
                                <tr>
                                    <td>{{ $ingresso->id }}</td>
                                    <!-- <td>{{ $ingresso->ingresso }}</td> -->
                                    <td>{{ $ingresso->setor->setor }}</td>
                                    <td>{{ $ingresso->sexo->sexo }}</td>
                                    <td>{{ $ingresso->lote->lote }}</td>
                                    <td>{{ $ingresso->entrada->entrada }}</td>
                                    <td>{{  'R$ '.number_format($ingresso->valor, 2, ',', '.') }}</td>
                                    <td><a href="{{ route('ingresso.edit', $ingresso->id) }}" class="btn btn-sm btn-primary" alt="Editar" title="Editar"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span></a></td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
