@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Gerenciamento de Lotes</div>

                <div class="panel-body">

                    @if (isset($lote) )
                        <form action="{{ route('lote.update', $lote->id) }}" method="POST" role="form">
                        {!! method_field('PUT') !!}
                    @else
                        <form action="{{ route('lote.store') }}" method="POST" role="form">
                    @endif

                        {!! csrf_field() !!}

                        <div class="form-group">
                            <label for="">Lote</label>
                            <input type="text" class="form-control" name="lote" value="{{ $lote->lote or '' }}" required>
                        </div>

                        <button type="submit" class="btn btn-success">Enviar</button>
                    </form>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
