@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Listagem de ingressos para registro de entrada </div>

                <div class="panel-body">

                    <div class="table-responsive">
                        <table class="table table-hover">
                            <thead>
                                <tr>
                                    <th width="60">ID</th>
                                    <!-- <th>Ingresso</th> -->
                                    <th>Setor</th>
                                    <th>Sexo</th>
                                    <th>Lote</th>
                                    <th>Tipo de Entrada</th>
                                    <th>Valor</th>
                                    <th width="80" class="text-center">Ações</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($ingressos as $ingresso)
                                <tr>
                                    <td>{{ $ingresso->id }}</td>
                                    <td>{{ $ingresso->setor->setor }}</td>
                                    <td>{{ $ingresso->sexo->sexo }}</td>
                                    <td>{{ $ingresso->lote->lote }}</td>
                                    <td>{{ $ingresso->entrada->entrada }}</td>
                                    <td>{{  'R$ '.number_format($ingresso->valor, 2, ',', '.') }}</td>
                                    <td class="text-center">
                                        <form action="{{ route('registro.store') }}" method="POST" role="form">
                                            {!! csrf_field() !!}
                                            <input type="hidden" class="hide" name="ingresso" value="{{ $ingresso->id }}">
                                            <button type="submit" class="btn btn-sm btn-success" alt="Registrar Entrada" title="Registrar Entrada"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span> Registrar Entrada</button>
                                        </form>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>

                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Relatório das entradas cadastradas</div>

                <div class="panel-body">

                    @include('alertas')

                    <div class="table-responsive">
                        <table class="table table-hover">
                            <thead>
                                <tr>
                                    <th>ID do Ingresso</th>
                                    <th width="200">Data do Registro</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($registros as $registro)
                                <tr>
                                    <td>{{ $registro->ingresso_id }}</td>
                                    <td>{{ $registro->created_at->format('d/m/Y  - H:i\h') }}</td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
