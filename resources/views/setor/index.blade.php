@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Gerenciamento de Setores <a href="{{ route('setor.create') }}" class="btn btn-xs btn-success pull-right" alt="Adicionar" title="Adicionar"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span> Adicionar</a></div>

                <div class="panel-body">

                    @include('alertas')

                    <div class="table-responsive">
                        <table class="table table-hover">
                            <thead>
                                <tr>
                                    <th width="60">ID</th>
                                    <th>Setor</th>
                                    <th width="80">Ações</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($setores as $setor)
                                <tr>
                                    <td>{{ $setor->id }}</td>
                                    <td>{{ $setor->setor }}</td>
                                    <td><a href="{{ route('setor.edit', $setor->id) }}" class="btn btn-sm btn-primary" alt="Editar" title="Editar"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span></a></td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
