@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Gerenciamento de Sexos</div>

                <div class="panel-body">

                    @if (isset($sexo) )
                        <form action="{{ route('sexo.update', $sexo->id) }}" method="POST" role="form">
                        {!! method_field('PUT') !!}
                    @else
                        <form action="{{ route('sexo.store') }}" method="POST" role="form">
                    @endif

                        {!! csrf_field() !!}

                        <div class="form-group">
                            <label for="">Sexo</label>
                            <input type="text" class="form-control" name="sexo" value="{{ $sexo->sexo or '' }}" required>
                        </div>

                        <button type="submit" class="btn btn-success">Enviar</button>
                    </form>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
