<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
})->name('root');



/*=============================================================
=            ROTAS DO LARAVEL PARA GESTÃO DE LOGIN            =
=============================================================*/

    //Auth::routes();

    Route::get('login', 'Auth\LoginController@showLoginForm')->name('login');
    Route::post('login', 'Auth\LoginController@login');
    Route::post('logout', 'Auth\LoginController@logout')->name('logout');

    // Registration Routes...
    Route::get('register', 'Auth\RegisterController@showRegistrationForm')->name('register');
    Route::post('register', 'Auth\RegisterController@register');

    Route::group(['prefix' => 'password'], function (){
        // Password Reset Routes...
        Route::get('/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('password.request');
        Route::post('/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');
        Route::get('/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password.reset');
        Route::post('/reset', 'Auth\ResetPasswordController@reset');
    });

/*=====  End of ROTAS DO LARAVEL PARA GESTÃO DE LOGIN  ======*/




/*=======================================================
=            ROTAS PARA O TESTE BOA DIVERSÃO            =
=======================================================*/

    Route::group(['middleware' => 'auth'], function (){
        Route::get('/home', 'HomeController@index')->name('home');
        Route::get('/teste', 'HomeController@index')->name('home_teste');

        Route::resource('/setor', 'SetorController');
        Route::resource('/sexo', 'SexoController');
        Route::resource('/lote', 'LoteController');
        Route::resource('/entrada', 'EntradaController');

        Route::resource('/ingresso', 'IngressoController');
        Route::resource('/registro', 'RegistroController');

    });

/*=====  End of ROTAS PARA O TESTE BOA DIVERSÃO  ======*/


